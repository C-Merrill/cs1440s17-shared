//
// Created by Stephen Clyde on 4/12/17.
//

#include <iostream>
#include "Widget.h"
#include "Gadget.h"

int main()
{
    Widget w1(1, "My Favorite Widget", 3124);
    w1.print(std::cout);

    Gadget g2(2, "Old Things", 235);
    g2.print(std::cout);
}