#include <iostream>
#include <exception>

void f()
{
    //throw std::out_of_range("Bad index");

    throw std::domain_error("Bad key");
}

void g()
{
    f();
}

void h()
{
    g();
}


int main() {

    try {
        h();
    }
    catch (std::out_of_range err)
    {
        std::cout << "Caught expected exception" << std::endl;
    }

    return 0;
}