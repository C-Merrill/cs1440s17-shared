//
// Created by Stephen Clyde on 3/29/17.
//

#include "Widget.h"

Widget::Widget(int width, int height) : m_width(width), m_height(height) {}

void Widget::print(std::ostream& out)
{
    out << "Widget: width=" << getWidth() << ", height=" << getHeight() << std::endl;
}
