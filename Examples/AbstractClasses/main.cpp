#include <iostream>

#include "Widget.h"
#include "Gadget.h"

int main() {
    Widget w1(10, 20);

    Gadget g1(235.35, 63.34);

    w1.print(std::cout);
    g1.print(std::cout);

    return 0;
}