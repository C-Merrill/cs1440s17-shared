//
// Created by Stephen Clyde on 3/29/17.
//

#ifndef ABSTRACTCLASSES_GADGET_H
#define ABSTRACTCLASSES_GADGET_H

#include "PrintableThing.h"

class Gadget : public PrintableThing {
private:
    double m_weight = 0;
    double m_cost = 0;

public:
    Gadget(double weight, double cost);
    void print(std::ostream& out);

    double getWeight() const { return m_weight; }
    double getCost() const { return m_cost; }
};



#endif //ABSTRACTCLASSES_GADGET_H
