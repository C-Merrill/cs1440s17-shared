//
// Created by Stephen Clyde on 3/29/17.
//

#ifndef ABSTRACTCLASSES_THING_H
#define ABSTRACTCLASSES_THING_H

#include <iostream>

// PrintableThing -- An example of an abstract class

class PrintableThing {
public:
    virtual void print(std::ostream& out) = 0;
};


#endif //ABSTRACTCLASSES_THING_H
