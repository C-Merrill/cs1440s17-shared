//
// Created by Stephen Clyde on 3/29/17.
//

#include "Gadget.h"

Gadget::Gadget(double weight, double cost) : m_weight(weight), m_cost(cost) {}

void Gadget::print(std::ostream& out)
{
    out << "Gadget: weight=" << getWeight() << ", cost=" << getCost() << std::endl;
}
